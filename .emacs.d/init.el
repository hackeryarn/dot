;; general
(cd "c:/Users/artem")
(setq default-directory "c:/Users/artem")
(setq inhibit-splash-screen t
      inhibit-startup-echo-area-message t)
(transient-mark-mode 1)
(tool-bar-mode -1)
(scroll-bar-mode -1)
(global-linum-mode 1)

(defalias 'list-buffers 'ibuffer-other-window)

(use-package try
  :ensure t)

;; macros
(fset 'frame-itemized
      [return ?\\ ?b ?e ?g ?i ?n ?\{ ?f ?r ?a ?m ?e ?\} return ?\\ ?b ?e ?g ?i ?n ?\{ ?i ?t ?e ?m ?i ?z ?e ?\} return return ?\\ ?e ?n ?d ?\{ ?i ?t ?e ?m ?i ?z ?e ?\} return ?\\ ?e ?n ?d ?  backspace ?\{ ?f ?r ?a ?m ?e ?\} return ?\C-p ?\C-p ?\C-p tab])

(fset 'frame-listing
   [?\\ ?b ?e ?g ?i ?n ?\{ ?f ?r ?a ?m ?e ?\} ?\[ ?f ?r ?a ?g ?i ?l ?e ?\] return ?\\ ?f ?r ?a ?m ?e ?t ?i ?t ?l ?e ?\{ ?\} return ?\\ ?b ?e ?g ?i ?n ?\{ ?l ?s ?t ?l ?i ?s ?t ?i ?n ?g ?\} return return ?\\ ?e ?n ?d ?\{ ?l ?s ?t ?l ?i ?s ?t ?i ?n ?g ?\} return ?\\ ?e ?n ?d ?\{ ?f ?r ?a ?m ?e ?\} tab ?\C-p ?\C-p ?\C-p ?\C-p right right right])

 
;; edit server
(use-package edit-server)

(when (require 'edit-server nil t)
  (setq edit-server-new-frame nil)
  (edit-server-start))

(when (and (require 'edit-server nil t) (daemonp))
  (edit-server-start))

(setq edit-server-url-major-mode-alist
      '(("github\\.com" . markdown-mode)))        

;; helm and swiper
(use-package counsel
  :ensure t)

(use-package swiper-helm
  :init (ivy-mode 1)
  :ensure t
  :config
  ;; not sure if this needs to be in :init or :config
  (setq ivy-use-virtual-buffers t)
  (define-key read-expression-map (kbd "C-r") 'counsel-expression-history)
  :bind (("\C-s" . swiper)
	 ("C-c C-r" . ivy-resume)
	 ("<f6>" . ivy-resume)
	 ("M-x" . counsel-M-x)
	 ("C-x C-f" . counsel-find-file)
	 ("<f1> f" . counsel-describe-function)
	 ("<f1> v" . counsel-describe-variable)
	 ("<f1> l" . counsel-load-library)
	 ("<f2> i" . counsel-info-lookup-symbol)
	 ("<f2> u" . counsel-unicode-char)
	 ("C-c g" . counsel-git):
	 ("C-c j" . counsel-git-grep)
	 ("C-c k" . counsel-ag)
	 ("C-x l" . counsel-locate)
	 ("C-S-o" . counsel-rhythmbox)))

;; ido setting
;; (require 'ido)
;; (ido-mode 1)
;; (setq ido-enable-flex-matching t)
;; (setq ido-everywhere t)
;; (setq ido-create-new-buffer 'always)
;; (setq ido-separator "\n")
;; (setq max-mini-window-height 0.5)

;; company
(add-hook 'after-init-hook 'global-company-mode)

;; paredit
(autoload 'enable-paredit-mode "paredit" "Turn on pseudo-structural editing of Lisp code." t)
(add-hook 'emacs-lisp-mode-hook       #'enable-paredit-mode)
(add-hook 'eval-expression-minibuffer-setup-hook #'enable-paredit-mode)
(add-hook 'ielm-mode-hook             #'enable-paredit-mode)
(add-hook 'lisp-mode-hook             #'enable-paredit-mode)
(add-hook 'lisp-interaction-mode-hook #'enable-paredit-mode)
(add-hook 'scheme-mode-hook           #'enable-paredit-mode)

;; paredit
(require 'paredit-everywhere)
(add-hook 'after-init-hook 'paredit-everywhere-mode)

;; org mode
(require 'org)

(use-package org-bullets
  :ensure t
  :config
  (add-hook 'org-mode-hook (lambda () (org-bullets-mode 1))))

(global-set-key "\C-cl" 'org-store-link)
(global-set-key "\C-ca" 'org-agenda)
(global-set-key "\C-cc" 'org-capture)
(global-set-key "\C-cb" 'org-iswitchb)

(setq org-todo-keywords '("TODO" "STARTED" "WAITING" "DONE"))

(setq org-agenda-include-diary t)
(setq org-agenda-include-all-todo t)
(setq org-agenda-include-deadlines t)
(setq org-indent-mode t)

(setq org-capture-templates
      '(("o" "Organizer" entry
	 (file+headline "c:/Users/artem/Dropbox/org/organizer.org" "Inbox")
	 "** TODO %?\n  %i\n")
	("O" "Organizer with link" entry
	 (file+headline "c:/Users/artem/Dropbox/org/organizer.org" "Inbox")
	 "** TODO %?\n  %i\n  %A\n")
	("w" "Work" entry
	 (file+headline "c:/Users/artem/Dropbox/org/work.org" "Inbox")
	 "** TODO %?\n  %i\n")
	("W" "Work with link" entry
	 (file+headline "c:/Users/artem/Dropbox/org/work.org" "Inbox")
	 "** TODO %?\n  %i\n  %A\n")
	("j" "Journal" entry
	 (file+datetree "c:/Users/artem/Dropbox/org/journal.org")
	 "* %?\n\nEntered on %U\n  %i\n")))

;; slime
(add-to-list 'exec-path "C:/Program Files/Steel Bank Common Lisp/1.3.6")
(setq inferior-lisp-program "sbcl")

;; which key
(use-package which-key
  :ensure t
  :config
  (which-key-mode))

(setq which-key-idle-delay 0.1)

;; ace-window
(use-package ace-window
  :ensure t
  :init
  (progn
    (global-set-key [remap other-window] 'ace-window)
    (custom-set-faces
     '(aw-leading-char-face
       ((t (:inherit ace-jump-face-foreground :height 3.0)))))
    ))

(setq aw-keys '(?a ?s ?d ?f ?g ?j ?k ?l))
