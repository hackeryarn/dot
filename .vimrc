:imap jk <Esc>
"basic
syntax on
filetype plugin indent on
set background=dark
"tabs
set tabstop=2
set expandtab
set softtabstop=2
set shiftwidth=2
set smarttab
set shiftround
set nojoinspaces
set number
colorscheme monokai 
set autoindent
set smartindent
